<?php

namespace App\Console\Commands;

use App\Http\Controllers\SalaryPaymentDateController;
use Illuminate\Console\Command;

class SalaryPaymentDateConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'salary-payment-date:tool';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Salary Payment Date tool';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Building a salary payment date tool!");

        $year = $this->ask('Enter year did you generate salary payment dates?');

        $this->info("Your requested year " . $year . " to generate CSV");

        $generateCSV = resolve( SalaryPaymentDateController::class )->generateCSV($year);

        $headersName = ['Month Name', 'Salary Payment Date', 'Bonus Date'];

        $this->table($headersName, $generateCSV);

        $filename = storage_path('app/'.$year.'.csv');;

        $handle = fopen($filename, 'w');

        fputcsv($handle, $headersName);

        $this->output->progressStart(count($generateCSV));

        foreach ($generateCSV as $list) {
            sleep(1);

            $month = $list['month'];
            $lastDate = $list['last_date'];
            $bonusDate = $list['bonus_date'];

            fputcsv($handle, array("$month", "$lastDate", "$bonusDate"));

            $this->output->progressAdvance();
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        $this->output->progressFinish();

        $this->info('CSV file path '. $filename);

        $this->line('Done');
    }
}
