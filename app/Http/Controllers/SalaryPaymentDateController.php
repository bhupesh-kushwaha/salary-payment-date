<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\SalaryPaymentDateInterface;
use App\Http\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SalaryPaymentDateController extends Controller implements SalaryPaymentDateInterface
{
    use HelperTrait;

    public function generateCSV($year) {
        $fileName = $year.'.csv';

        Storage::disk('local')->put($fileName, '');

        return $this->generateSalaryPaymentDateLists($year);
    }
}
