<?php

namespace App\Http\Interfaces;

interface SalaryPaymentDateInterface
{
    public function generateCSV($year);
}
