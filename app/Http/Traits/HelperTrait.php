<?php

namespace App\Http\Traits;

trait HelperTrait
{
    public function getWeekends() {
        return ['saturday', 'sunday'];
    }

    public function generateSalaryPaymentDateLists($year) {

        $monthsAndLastDay = [];

        foreach(range(1, 12, 1) as $month)
        {
            list($month, $last_date, $bonus_date) = $this->generateResponseArray($year, $month);

            $arr['month'] = $month;
            $arr['last_date'] = $last_date;
            $arr['bonus_date'] = $bonus_date;

            array_push($monthsAndLastDay, $arr);
        }

        return $monthsAndLastDay;
    }

    public function generateResponseArray($year, $month){
        $date = $year . "-" .$month;

        $lastDay = date('t', strtotime($date));

        $lastDate = $this->isLastDateWeekend( $year . "-" . $month . "-". $lastDay );

        $bonusDate = $this->generateBonusDate( $lastDate );

        return array(
            date('F', strtotime($date)),
            $lastDate,
            $bonusDate
        );
    }

    public function isLastDateWeekend($date) {

        $result = $this->isWeekDay($date);

        if( $result > 5 ) {
            $date = date('Y-m-d', strtotime($date . '-1 day'));

            $this->isLastDateWeekend($date);
        }

        return $date;
    }

    public function generateBonusDate($last_date) {
        $bonusDate = date('Y-m-d', strtotime($last_date . '+15 day'));

        return $this->isBonusDateWeekend($bonusDate, true);
    }

    public function isBonusDateWeekend($date, $stop = false) {

        $result = $this->isWeekDay($date);

        if( $result > 5 && $stop === false ) {
            $date = date('Y-m-d', strtotime($date . 'next Wednesday'));

            $this->isWeekend($date, true);
        }

        return $date;
    }

    public function isWeekDay($date){
        return date('N', strtotime($date));
    }
}
